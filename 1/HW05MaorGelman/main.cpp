#include "Menu.h"

#define EXIT 3
#define NEW_SHAPE 0
#define INFO_OR_MODIFY 1
#define DELETE_ALL 2


int main(void)
{
	/*variables definition*/
	int userChoice = 0;
	Menu menu;

	do
	{
		system("cls"); /*new screen*/

		menu.printOptions();
		cout << endl;
		
		/*getting user choice*/
		cin >> userChoice;
		userChoice = menu.getValidInput(userChoice); 

		system("cls");/*new screen*/

		/*checking what the user want to do*/
		switch (userChoice)
		{
		case NEW_SHAPE:
			menu.createNewShape();
			break;
		case INFO_OR_MODIFY:
			menu.getInfoOrModifyShape();
			break;
		case DELETE_ALL:
			menu.deleteAll();
			break;
		default:
			cout << "Bye :)" << endl;
		}

	} while (userChoice != EXIT); /*while the user does not want to exit the program*/

	system("pause");
	return 0;
}