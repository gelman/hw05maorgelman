#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):Polygon(type,name)
{
	this->_a = Point(a);
	this->_b = Point(b);
	this->_c = Point(c);

	/*pushing all the points to the vector*/
	(this->_points).push_back(a);
	(this->_points).push_back(b);
	(this->_points).push_back(c);
}


Triangle::~Triangle()
{
}


double Triangle::getArea() const
{
	/*calculating the triangle's area*/
	double s = (_a.distance(_b) + _a.distance(_c) + _b.distance(_c)) / 2;
	return sqrt(s*(s - _a.distance(_b))*(s - _a.distance(_c))*(s - _b.distance(_c)));
}

double Triangle::getPerimeter() const
{
	return this->_a.distance(this->_b) + this->_a.distance(this->_c) + this->_b.distance(this->_c);
}


void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) /*calculating the triangle's perimeter*/
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

void Triangle::move(const Point& other)
{
	/*poping the points from the vector*/
	(this->_points).pop_back();
	(this->_points).pop_back();
	(this->_points).pop_back();

	this->_a += other;
	this->_b += other;
	this->_c += other;

	/*pushing the points to the vector*/
	(this->_points).push_back(this->_a);
	(this->_points).push_back(this->_b);
	(this->_points).push_back(this->_c);
}
