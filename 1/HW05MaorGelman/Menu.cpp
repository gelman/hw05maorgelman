#include "Menu.h"

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define MAX_VALUE 3
#define MIN_VALUE 0
#define MOVE 0
#define DETAILS 1

Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::printOptions(void) const
{
	cout << "Enter 0 to add a new shape." << endl << "Enter 1 to modify or get information from a current shape." << endl;
	cout << "Enter 2 to delete all of the shapes." << endl << "Enter 3 to exit.";
}

int Menu::getValidInput(const int choice) const
{
	/*variables definition*/
	int userChoice = choice;

	/*checking the input*/
	while (userChoice > MAX_VALUE || userChoice < MIN_VALUE)
	{
		/*invalid input*/
		cout << "Invalid choice!" << endl;
		cout << "Your choice must be one of the next options only: 0,1,2,3" << endl;

		cin >> userChoice;
	}

	return userChoice;
}

void Menu::createNewShape(void)
{
	/*variables definition*/
	int choice = 0;

	/*getting the user choice*/
	cout << "Enter 0 to add a circle." << endl << "Enter 1 to add an arrow." << endl << "Enter 2 to add a triangle." << endl << "Enter 3 to add a rectangle." << endl;
	cin >> choice;
	choice = this->getValidInput(choice);

	/*checking what the user want to do*/
	switch (choice)
	{
		case CIRCLE:
			createCircle();
			break;
		case ARROW:
			createArrow();
			break;
		case TRIANGLE:
			createTriangle();
			break;
		default:
			createRectangle();
			
	}

}

void Menu::createCircle(void) 
{
	/*variables definition*/
	int x = 0, y = 0;
	double radius = 0;
	string name = "";

	/*getting the circle details*/
	cout << "Please enter X:" << endl;
	cin >> x;
	cout << "Please enter Y:" << endl;
	cin >> y;

	cout << "Please enter radius:" << endl;
	cin >> radius;

	/*checking if the radius is valid*/
	while (radius <= 0)
	{
		cout << "Radius must be bigger than 0" << endl;
		cout << "Please enter radius:" << endl;
		cin >> radius;
	}

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	/*new circle*/
	Circle* newShape = new Circle(Point(x,y), radius, "Circle", name);

	/*drawing the shape*/
	newShape->draw(*(this->_disp), *(this->_board));
	
	/*putting the new shape in the shapes vector*/
	_allTheShapes.push_back(newShape);
}


void Menu::createArrow(void)
{
	/*variables definition*/
	int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	string name = "";

	/*getting the arrow details*/
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;
	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	/*new arrow*/
	Arrow* newShape = new Arrow(Point(x1,y1), Point(x2,y2), "Arrow", name);

	/*drawing the shape*/
	newShape->draw(*(this->_disp), *(this->_board));

	/*putting the new shape in the shapes vector*/
	_allTheShapes.push_back(newShape);
}

void Menu::createTriangle(void)
{
	/*variables definition*/
	int x1 = 0, y1 = 0, x2 = 0, y2 = 0, x3 = 0 , y3 = 0;
	string name = "";

	/*getting the triangle details*/
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;
	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;
	cout << "Enter the X of point number: 3" << endl;
	cin >> x3;
	cout << "Enter the Y of point number: 3" << endl;
	cin >> y3;
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	/*checking if all the points on the same line*/
	if (y1 == y2 && y2 == y3)
	{
		cout << "The points entered create a line." << endl;
	}
	else
	{
		/*new triangle*/
		Triangle* newShape = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", name);

		/*drawing the shape*/
		newShape->draw(*(this->_disp), *(this->_board));

		/*putting the new shape in the shapes vector*/
		_allTheShapes.push_back(newShape);
	}
}

void Menu::createRectangle(void)
{
	/*variables definition*/
	int x = 0, y = 0;
	double length = 0, width = 0;
	string name = "";

	/*getting the rectangle details*/
	cout << "Enter the X of the to left corner:" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;
	cout << "Please enter the length of the shape:" << endl;
	cin >> length;
	cout << "Please enter the width of the shape:" << endl;
	cin >> width;
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	/*checking if the length and the width are valid*/
	if (length <= 0 || width <= 0)
	{
		cout << "Length or Width can't be 0 or smaller." << endl;;
	}
	else
	{
		/*new rectangle*/
		myShapes::Rectangle* newShape = new myShapes::Rectangle(Point(x,y), length, width, "Rectangle", name);

		/*drawing the shape*/
		newShape->draw(*(this->_disp), *(this->_board));

		/*putting the new shape in the shapes vector*/
		_allTheShapes.push_back(newShape);
	}

}

void Menu::deleteAll(void)
{
	/*variables definition*/
	int i = 0;

	/*erasing all the shapes from the screen*/
	for (i = 0; i < _allTheShapes.size(); i++)
	{
		_allTheShapes[i]->clearDraw(*(this->_disp), *(this->_board));
		delete _allTheShapes[i]; /*free the memory*/
		_allTheShapes[i] = nullptr;
	}

	/*poping all the shapes in the vector - we need to delete all the shapes*/
	while (_allTheShapes.size() > 0)
	{
		_allTheShapes.pop_back();
	}

}

void Menu::getInfoOrModifyShape(void)
{
	/*variables definition*/
	int i = 0, userChoice = 0, index = 0;


	if (_allTheShapes.size() != 0) /*if the vector is empty we don't have any details and we can't modify anything*/
	{
		for (i = 0; i < _allTheShapes.size(); i++) /*printing all the shapes we have*/
		{
			cout << "Enter " << i << " for " << _allTheShapes[i]->getName() << "(" << _allTheShapes[i]->getType() << ")" << endl;
		}

		/*getting the user choice*/
		cin >> userChoice;

		/*checking if the choice is valid*/
		while (userChoice > _allTheShapes.size() - 1 || userChoice < 0)
		{
			cout << "Invalid input, Try again!" << endl;
			cin >> userChoice;
		}

		index = userChoice; /*the choice is the index in the vector*/

		cout << "Enter 0 to move the shape" << endl;
		cout << "Enter 1 to get its details." << endl;
		cout << "Enter 2 to remove the shape." << endl;

		/*getting the user choice*/
		cin >> userChoice;

		/*checking if the input is valid*/
		while (userChoice != 0 && userChoice != 1 && userChoice != 2)
		{
			cout << "Invalid input, Try again!" << endl;
			cin >> userChoice;
		}

		/*checking what the user want to do*/
		switch (userChoice)
		{
			case MOVE:
				moveShape(index);
				break;
			case DETAILS:
				_allTheShapes[index]->printDetails();
				cout << endl;
				system("pause");
				break;
			default:
				deleteShape(index);

		}

	}
}

void Menu::moveShape(int index)
{
	/*variables definition*/
	double movingScaleX = 0, movingScaleY = 0;

	system("cls"); /*new screen*/

	/*getting the moving scales*/
	cout << "Please enter the X moving scale :";
	cin >> movingScaleX;
	cout << "Please enter the Y moving scale :";
	cin >> movingScaleY;

	/*clear the board*/
	deleteAllTheShapesFromTheBoard();

	/*moving the shape*/
	_allTheShapes[index]->move(Point(movingScaleX, movingScaleY));

	/*drawing the shapes again*/
	drawAllTheShapes();
}

void Menu::deleteShape(int index)
{
	/*clear the board*/
	deleteAllTheShapesFromTheBoard();

	delete _allTheShapes[index]; /*free the memory*/
	_allTheShapes[index] = nullptr;

	_allTheShapes.erase(_allTheShapes.begin() + index); /*delete the value from the vector*/

    /*drawing the shapes again*/
	drawAllTheShapes();
}

void Menu::deleteAllTheShapesFromTheBoard(void)
{
	/*variables definition*/
	int i = 0;

	/*clear the screen*/
	for (i = 0; i < _allTheShapes.size(); i++)
	{
		_allTheShapes[i]->clearDraw(*(this->_disp), *(this->_board));
	}
}

void Menu::drawAllTheShapes(void)
{
	/*variables definition*/
	int i = 0;

	/*drawing the shapes*/
	for (i = 0; i < _allTheShapes.size(); i++)
	{
		_allTheShapes[i]->draw(*this->_disp, *this->_board);
	}
}