#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name):Polygon(type,name)
{
	this->_a = Point(a);
	this->_length = length;
	this->_width = width;

	/*pushing the points to the vector*/
	(this->_points).push_back(a);
	(this->_points).push_back(a + Point(length,width));
}


myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width; /*rectangle area*/
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * (this->_length + this->_width); /*rectangle perimeter*/
}



void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}

void myShapes::Rectangle::move(const Point& other)
{
	/*poping the points from the vector*/
	(this->_points).pop_back();
	(this->_points).pop_back();

	this->_a += other;

	/*pushing the points to the vector*/
	(this->_points).push_back(this->_a);
	(this->_points).push_back(this->_a + Point(this->_length, this->_width));
}
