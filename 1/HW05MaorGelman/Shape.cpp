#include "Shape.h"
#include <string>

string Shape::getName() const
{
	return this->_name;
}

string Shape::getType() const
{
	return this->_type;
}

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	/*printing all the shape details*/
	cout << "name: " << this->_name << "  type: " << this->_type << "  area: " << this->getArea() << "  perimeter: " << this->getPerimeter() << endl;
}

