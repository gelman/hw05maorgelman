#include "Point.h"
#include <math.h> 

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{
}


double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}


double Point::distance(const Point& other) const
{
	return sqrt(pow((this->_x - other._x), 2.0) + pow((this->_y - other._y), 2.0)); /*calculating distance*/
}

Point Point::operator+(const Point& other) const
{
	Point p(this->_x + other._x , this->_y + other._y);
	return p;
}

Point& Point::operator+=(const Point& other)
{
	/*adding the values of the other point to ours point*/
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}


