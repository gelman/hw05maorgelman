#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	void printOptions(void) const;
	int getValidInput(const int choice) const;
	void createNewShape(void);
	void createCircle(void);
	void createArrow(void);
	void createTriangle(void);
	void createRectangle(void);
	void deleteAll(void);
	void getInfoOrModifyShape(void);
	void deleteAllTheShapesFromTheBoard(void);
	void moveShape(int index);
	void deleteShape(int index);
	void drawAllTheShapes(void);

protected:
	std::vector<Shape *> _allTheShapes;


private:
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;

};